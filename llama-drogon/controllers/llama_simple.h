#pragma once

#include <sstream>
#include <iostream>

#include <drogon/HttpController.h>

#include "common.h"
#include "llama.h"

using namespace drogon;

namespace llama
{
class simple : public drogon::HttpController<simple>
{
  public:
    simple();
    ~simple();
    METHOD_LIST_BEGIN
    // use METHOD_ADD to add your custom processing function here;
    METHOD_ADD(simple::GetAllModels, "/get-all-models", Get);
    METHOD_ADD(simple::LoadMode, "/load_model", Post);
    METHOD_ADD(simple::Chat, "/chat", Post);

    METHOD_LIST_END
    void GetAllModels(const HttpRequestPtr& req, std::function<void (const HttpResponsePtr &)> &&callback);
    void LoadMode(const HttpRequestPtr& req, std::function<void (const HttpResponsePtr &)> &&callback);
    void Chat(const HttpRequestPtr& req, std::function<void (const HttpResponsePtr &)> &&callback);
  private:
    llama_model * model;
    llama_context * ctx;
    llama_batch batch;
    gpt_params params;

    std::vector<std::string> list_files(std::string folderPath);
    bool load_model(std::string model_path);
    std::string chat_complete(std::string prompt, int n_len);
    std::vector<std::string> split(const std::string& str, char delimiter);
};
}
